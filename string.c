/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:05:10 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 11:18:15 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

int	string_new(t_string **s)
{
	char	*p;

	*s = malloc(sizeof(t_string));
	if (!*s)
		return (0);
	p = malloc(sizeof(char));
	if (!p)
	{
		free(*s);
		return (0);
	}
	*p = '\0';
	(*s)->str = p;
	(*s)->sz = 1;
	return (1);
}

void	string_delete(t_string **s)
{
	if ((*s)->sz)
		free((*s)->str);
	free(*s);
	*s = 0;
}

int	string_append(t_string *s, char *txt, size_t n)
{
	char	*p;

	p = malloc(sizeof(char) * (s->sz + n));
	if (!p)
		return (0);
	_strcpy(p, s->str);
	_strncat(p, txt, n);
	free(s->str);
	s->str = p;
	s->sz += n;
	return (1);
}
