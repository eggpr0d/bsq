/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readmap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:06:33 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 12:37:17 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

int	mapinfo_getinfo(t_mapinfo *m, char **s)
{
	char	*p;
	char	*start;

	start = *s;
	if (!_isdigit(*start))
		return (0);
	p = start;
	while (*p && *p != '\n')
		++p;
	if (!*p)
		return (0);
	if (p - start != 4)
		return (0);
	m->numlines = *start - 48;
	m->emptychar = *(start + 1);
	m->obstaclechar = *(start + 2);
	m->fullchar = *(start + 3);
	if (m->emptychar == m->obstaclechar || m->emptychar == m->fullchar)
		return (0);
	if (m->obstaclechar == m->fullchar)
		return (0);
	if (!*(p + 1))
		return (0);
	*s = p + 1;
	return (mapinfo_countcols(m, *s));
}

int	mapinfo_preparemap(t_mapinfo *m)
{
	int		i;

	m->map = malloc(sizeof(char *) * m->numlines);
	if (!m->map)
		return (0);
	i = 0;
	while (i < m->numlines)
		m->map[i++] = 0;
	i = 0;
	while (i < m->numlines)
	{
		m->map[i] = malloc(sizeof(char) * m->numcols);
		if (!m->map[i])
			return (0);
		++i;
	}
	return (1);
}

int	mapinfo_getmap(t_mapinfo *m, char **s)
{
	char	*start;
	int		i;
	int		j;

	start = *s;
	i = 0;
	while (i < m->numlines)
	{
		while (**s && **s != '\n')
			++(*s);
		if (!**s)
			return (0);
		if (*s - start != m->numcols)
			return (0);
		j = 0;
		while (j < m->numcols)
		{
			m->map[i][j] = *(start + j);
			++j;
		}
		start = ++(*s);
		++i;
	}
	return (1);
}

int	mapinfo_fromstring(t_mapinfo *m, char *s)
{
	if (!mapinfo_getinfo(m, &s))
		return (0);
	if (!mapinfo_preparemap(m))
		return (0);
	if (!mapinfo_getmap(m, &s))
		return (0);
	if (*++s)
		return (0);
	if (!mapinfo_checkmap(m))
		return (0);
	return (1);
}

void	readmap(char *s)
{
	t_mapinfo	*mapinfo;

	mapinfo_new(&mapinfo);
	if (!mapinfo_fromstring(mapinfo, s))
	{
		mapinfo_delete(&mapinfo);
		return (maperror());
	}
	searchmap(mapinfo);
	if (mapinfo->i != -1)
	{
		modmap(mapinfo);
		printmap(mapinfo);
	}
	mapinfo_delete(&mapinfo);
}
