/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:02:25 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 11:27:09 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

void	openfd(int fd)
{
	t_string	*txt;
	int			sz;
	char		buf[128];

	txt = 0;
	string_new(&txt);
	sz = 1;
	while (sz)
	{
		sz = read(fd, buf, 128);
		if (sz > 0)
		{
			string_append(txt, buf, sz);
		}
		if (sz < 0)
		{
			string_delete(&txt);
			return (maperror());
		}
	}
	readmap(txt->str);
	string_delete(&txt);
}

void	openfile(char *fpath)
{
	int		fd;

	fd = open(fpath, O_RDONLY);
	if (fd < 0)
		return (maperror());
	openfd(fd);
	close(fd);
}

void	fromstdin(void)
{
	openfd(0);
}

void	fromfiles(int cnt, char *filepaths[])
{
	int		i;

	i = 0;
	while (i < cnt)
	{
		openfile(filepaths[i]);
		++i;
	}
}

int	main(int argc, char *argv[])
{
	if (argc == 1)
	{
		fromstdin();
	}
	else
	{
		fromfiles(argc - 1, &argv[1]);
	}
	return (0);
}
