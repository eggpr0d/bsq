/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printmap.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:04:27 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 11:15:45 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

void	printmap(t_mapinfo *m)
{
	int		i;
	int		j;

	i = 0;
	while (i < m->numlines)
	{
		j = 0;
		while (j < m->numcols)
		{
			_putchar(m->map[i][j]);
			++j;
		}
		_putchar('\n');
		++i;
	}
}
