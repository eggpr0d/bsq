/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   h.h                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 10:59:35 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 12:55:13 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_H
# define H_H

# include <assert.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

# define MAX(x, y)	((x) <= (y) ? (y) : (x))
# define MIN(x, y)	((x) >= (y) ? (y) : (x))

typedef struct s_string		t_string;

struct s_string
{
	char	*str;
	size_t	sz;
};

int		string_append(t_string *s, char *txt, size_t n);
void	string_delete(t_string **s);
int		string_new(t_string **s);


typedef struct s_mapinfo	t_mapinfo;

struct s_mapinfo
{
	int		numlines;
	int		numcols;
	char	emptychar;
	char	obstaclechar;
	char	fullchar;
	char	**map;
	int		i;
	int		j;
	int		k;
};

int		mapinfo_checkmap(t_mapinfo *m);
int		mapinfo_countcols(t_mapinfo *m, char *s);
void	mapinfo_delete(t_mapinfo **m);
int		mapinfo_new(t_mapinfo **m);

typedef struct s_square		t_square;

struct s_square
{
	int		x;
	int		y;
	int		sz;
};

int		square_isok(t_mapinfo *m, t_square *sq);

void	maperror(void);
int		modmap(t_mapinfo *m);
void	printmap(t_mapinfo *m);
void	readmap(char *s);
void	searchmap(t_mapinfo *m);

int		_haschar(char *charset, char c);
int		_isdigit(char c);
void	_print(char *msg);
void	_putchar(char c);
int		_strcmp(char *s1, char *s2);
char	*_strcpy(char *s1, char *s2);
int		_strlen(char *s);
char	*_strncat(char *s1, char *s2, size_t n);




#endif
