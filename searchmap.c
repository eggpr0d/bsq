/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   searchmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:55:26 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 13:27:35 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

void	markmap(t_mapinfo *m, int i, int j, int k)
{
	if (m->k < k)
	{
		m->i = i;
		m->j = j;
		m->k = k;
	}
}

int	modmap(t_mapinfo *m)
{
	int		i;
	int		j;

	i = m->i;
	while (i < (m->i + m->k))
	{
		j = m->j;
		while (j < (m->j + m->k))
		{
			m->map[i][j] = m->fullchar;
			++j;
		}
		++i;
	}
	return (1);
}

int	_searchmap(t_mapinfo *m, int i, int j, int k)
{
	t_square	sq;

	sq.x = i;
	sq.y = j;
	sq.sz = k;
	if (square_isok(m, &sq))
		return (1);
	return (0);
}

void searchmap(t_mapinfo *m)
{
	int			i;
	int			j;
	int			k;

	i = 0;
	while (i < m->numlines)
	{
		j = 0;
		while (j < m->numcols)
		{
			k = MIN(m->numlines, m->numcols) - MAX(i, j);
			while (k > 0)
			{//printf("%d %d %d\n",i,j,k);
				if (_searchmap(m, i, j, k))
					markmap(m, i, j, k);
				--k;
			}
			++j;
		}
		++i;
	}
}
