/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mapinfo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:04:00 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 12:31:34 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

int	mapinfo_new(t_mapinfo **m)
{
	*m = malloc(sizeof(t_mapinfo));
	if (!m)
		return (1);
	(*m)->map = 0;
	(*m)->i = -1;
	(*m)->j = -1;
	(*m)->k = -1;
	return (0);
}

void	mapinfo_delete(t_mapinfo **m)
{
	int		i;

	if ((*m)->map)
	{
		i = 0;
		while (i < (*m)->numlines)
		{
			if ((*m)->map[i])
				free((*m)->map[i]);
			++i;
		}
		free((*m)->map);
	}
	free(*m);
	*m = 0;
}

int	mapinfo_checkchar(t_mapinfo *m, char c)
{
	if (m->emptychar == c || m->obstaclechar == c)
		return (1);
	return (0);
}

int	mapinfo_checkmap(t_mapinfo *m)
{
	int		i;
	int		j;

	i = 0;
	while (i < m->numlines)
	{
		j = 0;
		while (j < m->numcols)
		{
			if (!mapinfo_checkchar(m, m->map[i][j]))
				return (0);
			++j;
		}
		++i;
	}
	return (1);
}

int	mapinfo_countcols(t_mapinfo *m, char *s)
{
	char	*start;

	if (m->numlines < 1)
		return (0);
	start = s;
	while (*s && *s != '\n')
		++s;
	if (!*s)
		return (0);
	if (s - start < 1)
		return (0);
	m->numcols = s - start;
	return (1);
}
