# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/08/01 10:58:45 by pmarquis          #+#    #+#              #
#    Updated: 2022/08/01 11:59:47 by pmarquis         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC := gcc
CFLAGS := -Wall -Wextra #-Werror

NAME = bsq

OBJ = haschar.o \
	isdigit.o \
	main.o \
	maperror.o \
	mapinfo.o \
	print.o \
	printmap.o \
	putchar.o \
	readmap.o \
	searchmap.o \
	square.o \
	strcmp.o \
	strcpy.o \
	string.o \
	strlen.o \
	strncat.o


$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $@

.c.o: h.h
	$(CC) $(CFLAGS) -c -o $(<:.c=.o) $<

all: $(NAME)

clean:
	rm -f *.o

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re

