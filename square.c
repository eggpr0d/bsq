/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <stan@astrorigin.com>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/01 11:43:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/08/01 13:21:09 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h.h"

int	square_isvalid(t_mapinfo *m, t_square *sq)
{
	if (sq->x < 0 || sq->x > m->numcols - 1)
		return (0);
	if (sq->y < 0 || sq->y > m->numlines - 1)
		return (0);
	if (sq->sz > m->numcols || sq->sz > m->numlines)
		return (0);
	return (1);
}

int	square_isok(t_mapinfo *m, t_square *sq)
{
	int		i;
	int		j;
assert(square_isvalid(m, sq));
	if (!square_isvalid(m, sq))
		return (0);
	i = 0;
	while (i < sq->sz)
	{
		j = 0;
		while (j < sq->sz)
		{
			if (m->map[sq->x + i][sq->y + j] == m->obstaclechar)
			{
				return (0);
			}
			++j;
		}
		++i;
	}printf("%d %d %d\n",sq->x,sq->y,sq->sz);
	return (1);
}
